package com.example.cyut.connestiondemo.Connection;

import android.content.Context;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CYUT on 2018/7/12.
 */

public class Connection extends BaseAsyncHttpClient {
    private static String SELECT_URL ="http://163.17.21.69:8080/cyut/post/select.php";
    private static String INSERT_URL ="http://163.17.21.69:8080/cyut/post/insert.php";
    private static String UPDATE_URL ="http://163.17.21.69:8080/cyut/post/update.php";
    private static String DELETE_URL ="http://163.17.21.69:8080/cyut/post/delete.php";
    public static void select (Context context, String id, final MyCallBack myCallBack){
        RequestParams params = new RequestParams();
        params.add("id", id);
        mClient.post(context,SELECT_URL,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getBoolean("status")){
                        myCallBack.onSuccess(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public static void select (Context context, final MyCallBack myCallBack){
        RequestParams params = new RequestParams();
        mClient.post(context,SELECT_URL,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getBoolean("status")){
                        myCallBack.onSuccess(response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void insert (Context context, String name, String phone, String address,
                               final MyCallBack myCallBack){
        RequestParams params = new RequestParams();
        params.add("name",name);
        params.add("phone",phone);
        params.add("address",address);
        mClient.post(context,INSERT_URL,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getBoolean("status")){
                        myCallBack.onSuccess();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
