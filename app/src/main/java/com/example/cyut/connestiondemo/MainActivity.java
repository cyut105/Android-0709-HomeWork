package com.example.cyut.connestiondemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cyut.connestiondemo.Connection.Connection;
import com.example.cyut.connestiondemo.Connection.MyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private enum SqlStatus{
        SELECT,
        INSERT,
        UPDATE,
        DELETE
    }

    //region 變數
    private Button button1,button2,button3,button4,button5,button6,button7,button8;
    private LinearLayout lay1,lay2;
    private EditText editText1,editText2,editText3,editText4;
    private TextView textView5;
    private SqlStatus sqlStatus;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findView();
        setListener();
    }

    private void findView(){
        button1=(Button)findViewById(R.id.button1);
        button2=(Button)findViewById(R.id.button2);
        button3=(Button)findViewById(R.id.button3);
        button4=(Button)findViewById(R.id.button4);
        button5=(Button)findViewById(R.id.button5);
        button6=(Button)findViewById(R.id.button6);
        button7=(Button)findViewById(R.id.button7);
        button8=(Button)findViewById(R.id.button8);

        lay1=(LinearLayout)findViewById(R.id.lay1);
        lay2=(LinearLayout)findViewById(R.id.lay2);

        editText1=(EditText)findViewById(R.id.editText1);
        editText2=(EditText)findViewById(R.id.editText2);
        editText3=(EditText)findViewById(R.id.editText3);
        editText4=(EditText)findViewById(R.id.editText4);

        textView5=(TextView)findViewById(R.id.textView5);
    }

    private void setListener(){
        button1.setOnClickListener(onClickListener);
        button2.setOnClickListener(onClickListener);
        button3.setOnClickListener(onClickListener);
        button4.setOnClickListener(onClickListener);
        button5.setOnClickListener(onClickListener);
        button6.setOnClickListener(onClickListener);
        button7.setOnClickListener(onClickListener);
        button8.setOnClickListener(onClickListener);
    }

    //region onClickListener
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editText1.getText().toString();
            String phone = editText2.getText().toString();
            String address = editText3.getText().toString();
            String id = editText4.getText().toString();
            switch (view.getId()){
                case R.id.button1:
                    onSelect();
                    break;
                case R.id.button2:
                    onInsert();
                    break;
                case R.id.button3:
                    onUpdate();
                    break;
                case R.id.button4:
                    onDelete();
                    break;
                case R.id.button5:
                    onConnection(name,phone,address);
                    break;
                case R.id.button6:
                    lay1.setVisibility(View.GONE);
                    break;
                case R.id.button7:
                    onConnection(id);
                    break;
                case R.id.button8:
                    lay2.setVisibility(View.GONE);
                    break;
            }
        }
    };
    //endregion

    //region 點擊處理
    private void onSelect(){
        sqlStatus=SqlStatus.SELECT;
        lay1.setVisibility(View.GONE);
        lay2.setVisibility(View.VISIBLE);
    }
    private void onInsert(){
        sqlStatus=SqlStatus.INSERT;
        lay1.setVisibility(View.VISIBLE);
        lay2.setVisibility(View.GONE);
    }
    private void onUpdate(){
        sqlStatus=SqlStatus.UPDATE;
        lay1.setVisibility(View.VISIBLE);
        lay2.setVisibility(View.GONE);
    }
    private void onDelete(){
        sqlStatus=SqlStatus.DELETE;
        lay1.setVisibility(View.GONE);
        lay2.setVisibility(View.VISIBLE);
    }
    //endregion

    //region 連線
    private void onConnection(String id){
        if(sqlStatus.equals(SqlStatus.SELECT)){
            Log.d("onConnection","SELECT");
            if(!id.equals("")){
                Connection.select(MainActivity.this,id,backSelect);
            }else {
                Connection.select(MainActivity.this,backSelect);
            }
        }
        if(sqlStatus.equals(SqlStatus.DELETE)){
            Log.d("onConnection","DELETE");
        }
    }
    private void onConnection(String name,String phone,String address){
        if(sqlStatus.equals(SqlStatus.INSERT)){
            Log.d("onConnection","INSERT");
            Connection.insert(MainActivity.this,name,phone,address,backInsert);
        }
        if(sqlStatus.equals(SqlStatus.UPDATE)){
            Log.d("onConnection","UPDATE");
        }
    }
    //endregion

    //region 回傳
    //region 搜尋回傳
    private MyCallBack backSelect = new MyCallBack(){
        @Override
        public void onSuccess(JSONObject response) {
            super.onSuccess(response);
            Log.d("backData",response.toString());
            try {
                StringBuilder textShow= new StringBuilder("-------------------------------------\n");
                JSONArray data =response.getJSONArray("data");

                for (int i =0;i<data.length();i++){

                    String id = data.getJSONObject(i).optString("id");
                    String name = data.getJSONObject(i).optString("name");
                    String phone = data.getJSONObject(i).optString("phone");
                    String address = data.getJSONObject(i).optString("address");
                    textShow.append(" ID: ")
                            .append(id)
                            .append("\n姓名: ")
                            .append(name)
                            .append("\n電話: ")
                            .append(phone)
                            .append("\n地址: ")
                            .append(address).append("\n");
                    textShow.append("-------------------------------------\n");

                }
                textView5.setText(textShow.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    //endregion

    //region 新增回傳
    private MyCallBack backInsert = new MyCallBack(){
        @Override
        public void onSuccess() {
            Toast.makeText(MainActivity.this,"新增成功\n請重新查詢資料",Toast.LENGTH_SHORT).show();
        }
    };
    //endregion

    //region 修改回傳
    private MyCallBack backUpdate = new MyCallBack(){

    };
    //endregion

    //region 刪除回傳
    private MyCallBack backDelete = new MyCallBack(){

    };
    //endregion
    //endregion
}
